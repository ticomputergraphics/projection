from OpenGL.GL import *
from OpenGL.GLUT import *
import math

width = 1080
height = 720

#class to wrap the methods needed for isometric calculation
class Isometric():

	#method to multiply a matrix with a vector
	def mult_matrix_vector(self, matrix, vector):
		return_matrix = [0] * len(matrix)
		if len(matrix[0]) == len(vector):
			for i in range (0, len(matrix)):
				for j in range(0, len(vector)):
					return_matrix[i] += matrix[i][j] * vector[j]
		return return_matrix

	#method to multipy a matrix with a matrix
	def mult_matrix_matrix(self, matrix_one, matrix_two):
		return_matrix = []
		for i in range (0, len(matrix_one)):
			return_matrix.append([])
			for j in range (0, len(matrix_two[0])):
				return_matrix[i].append(0)

		if len(matrix_one[0]) == len(matrix_two):
			for h in range (0, len(matrix_one[0])):
				for i in range (0, len(matrix_one)):
					value = 0
					for j in range(0, len(matrix_one[0])):
						value += matrix_one[i][j] * matrix_two[j][h]
					return_matrix[i][h] = value
			return return_matrix

	def isometricisize(self, coordinate_list, rotation):
		isometric_coordinates = []
		alpha = math.asin(math.tan(math.radians(30)))
		beta = math.radians(45 + rotation)
		projection_matrix_one = ((math.cos(beta), 0, -1 * math.sin(beta)),
								 (0, 1, 0),
								 (math.sin(beta), 0, math.cos(beta)))
		projection_matrix_two = ((1, 0, 0),
								(0, math.cos(alpha), math.sin(alpha)),
								(0, -1 * math.sin(alpha), math.cos(alpha)))
		combined_matrix = self.mult_matrix_matrix(projection_matrix_two, projection_matrix_one)
		for coordinate in coordinate_list:
			matrix_calc = self.mult_matrix_vector(combined_matrix, coordinate)
			isometric_coordinates.append((matrix_calc[0] + 0.5 * width, matrix_calc[1] + 0.5 * height))
		return isometric_coordinates

class Lines:

	def __init__(self, sizeX, sizeY):
		self.sizeX = sizeX
		self.sizeY = sizeY
		self.lines = []
		self.iso = Isometric()
		self.cube = Cube(0, 0, 0, 120)
		self.rotation = 0
		glutInit()
		glutInitDisplayMode(GLUT_RGB)
		glutInitWindowSize(sizeX, sizeY)
		glutCreateWindow("Lines".encode("ascii"))
		glOrtho(0, sizeX, sizeY, 0, -1, 1)
		glutDisplayFunc(self.display)
		#added timer func that will update after 2 seconds
		glutTimerFunc(2, self.updateLines, 1)
		glutKeyboardFunc(self.end)

	def addLine(self, p1, p2):
		x1, y1 = p1
		x2, y2 = p2
		if 0 <= x1 < self.sizeX and \
		   0 <= y1 < self.sizeY and \
		   0 <= x2 < self.sizeX and \
		   0 <= y1 < self.sizeY:
			self.lines.append((x1, y1, x2, y2))

	#updates the lines from the cube
	def updateLines(self, integer):
		#resets amount of lines
		self.lines = []
		#makes the isometric representation for an array of coordinates taking the current rotation into account
		isometric_coordinates = self.iso.isometricisize(self.cube.return_vertex_coordinates(), self.rotation)
		#draws lines between the calculated coordinates
		for edge in self.cube.return_edges():
			self.addLine(isometric_coordinates[edge[0]], isometric_coordinates[edge[1]])
		#increases rotation for one
		self.rotation += 1
		glutPostRedisplay()
		#set a timer to call this function again
		glutTimerFunc(25, self.updateLines, 1)


	def display(self):
		glClear(GL_COLOR_BUFFER_BIT)
		glColor(1, 1, 1)
		glBegin(GL_LINES)
		for i in self.lines:
			x1, y1, x2, y2 = i
			#glColor3f(0, 255, 0) sets color to green for effects
			glVertex(x1, y1)
			glVertex(x2, y2)
		glEnd()
		glFlush()

	def end(self, key, x, y):
		exit()

	def draw(self):
		glutMainLoop()

class Cube:
	# vertices related from the middle of the cube
	vertices = [[-1, -1, -1],
			   [1, -1, -1],
			   [1, 1, -1],
			   [-1, 1, -1],
			   [-1, -1, 1],
			   [1, -1, 1],
			   [1, 1, 1],
			   [-1, 1, 1]]
			   #[0, -2, 1], add these 2 lines to make it into a house
			   #[0, -2, -1]]

	# edges to show which vertex connects to which other vertex
	edges = ((0, 1),
			(1, 2),
			(2, 3),
			(0, 3),
			(0, 4),
			(1, 5),
			(2, 6),
			(3, 7),
			(4, 5),
			(5, 6),
			(6, 7),
			(4, 7))
			#(8, 9), also add these 5 lines to make it into a house
			#(4, 8),
			#(5, 8),
			#(0, 9),
			#(1, 9))

	def __init__(self, x, y, z, s):
		self.x = x # x coordinate
		self.y = y # y coordinate
		self.z = z # z coordinate
		self.s = s # s (scale)

	# Converts vertices to coordinates in the world
	def return_vertex_coordinates(self):
		return_coordinates = []
		for vertex in self.vertices:
			new_x = vertex[0] * 0.5 * self.s + self.x
			new_y = vertex[1] * 0.5 * self.s + self.y
			new_z = vertex[2] * 0.5 * self.s + self.z
			return_coordinates.append((new_x, new_y, new_z))
		return return_coordinates

	#returns the list with edgess
	def return_edges(self):
		return self.edges

l = Lines(width, height)
l.draw()

#data for testing the matrix multiplier function.
#matrix_one = [[4, 0, 3], [1, -1, 7], [-3, 3, 2]]
#matrix_two = [[-2, 3, 1], [2, -3, -5], [4, 0, 7]]
#print(mult_matrix_matrix(matrix_one, matrix_two))
